import { visit } from "../lib/test-helpers"
import { screen, waitForElementToBeRemoved } from "@testing-library/react"
import makeServer from "../server"
import userEvent from "@testing-library/user-event"

// Since each test starts and stops our base Mirage server, we can use Jest's beforeEach and afterEach hooks to clean this up:

let server

beforeEach(() => {
  server = makeServer("test")
})

afterEach(() => {
  server.shutdown()
})

// Our project is already set up with Jest and Testing Library. We've also provided a visit(url) helper that renders our Reminders app at a given URL.

// We use test environment in server.js for running tests.

  // The test environment sets the timing to 0 so that our tests run fast; it hides Mirage's logging so your test output remains clean; and most importantly, it skips the seeds() hook.

  // So, we get to reuse all of our models, serializers, factories and routes, but keep the seeds() data set apart for development mode. In testing, we will use each test to set up the data for our server in exactly the state we need for that test.

  // For our current test, we actually want the database to be empty, so we just need to start the server without creating any extra data. That allows Mirage to handle the GET API request correctly, and it responds with an empty dataset. And now our "All done" message assertion is passing, which is exactly what we want for this test.

test("it shows a message when there are no reminders", async () => {

  // bring our Mirage server into the test and pass in test environment
  
  // let server = makeServer("test")

  // We can't call makeServer in every test without cleaning it up.
  
    // Mirage uses the Pretender library under the hood, and Pretender is telling us we have two servers that are colliding with each other. We need to use the server.shutdown() method to clean up after every test.

    // We can do that by first opening up our server.js file, and making sure our makeServer function returns the server instance. And then back in our test, we can assign the return value of makeServer to a local variable, and use that to call server.shutdown() at the end of each test.

    // We can use our new local server variable to seed our server directly in our test, in exactly the same way that we do within our seeds() hook.

    // We are going to refactor and use Jest's beforeEach and afterEach hooks before our test definitions to clean thess up.

  visit("/")
  await waitForElementToBeRemoved(() => screen.getByText("Loading..."))

  expect(screen.getByText("All done!")).toBeInTheDocument()

  // server.shutdown()
})

test("it shows existing reminders", async () => {
  
  // let server = makeServer("test")

  server.create("reminder", { text: "Walk the dog" })
  server.create("reminder", { text: "Take out the trash" })
  server.create("reminder", { text: "Work out" })

  visit("/")
  await waitForElementToBeRemoved(() => screen.getByText("Loading..."))

  // As you can see, each test gives us an isolated place to alter the state of our Mirage server for the sake of whatever scenario we're testing. Since we cleanup after each test, none of these alterations leak from one test into another.

  expect(screen.getByText("Walk the dog")).toBeInTheDocument()
  expect(screen.getByText("Take out the trash")).toBeInTheDocument()
  expect(screen.getByText("Work out")).toBeInTheDocument()

  // server.shutdown()
})

test("it can add a reminder to a list", async () => {
  let list = server.create("list")

  // If you want to get a sense of the rendered output at this point, you could use the ?open query param to make sure the app's sidebar is open, and call screen.debug() to see our List in the output:

  visit(`/${list.id}?open`)
  await waitForElementToBeRemoved(() => screen.getByText("Loading..."))

  userEvent.click(screen.getByTestId("add-reminder"))
  await userEvent.type(screen.getByTestId("new-reminder-text"), "Work out")
  userEvent.click(screen.getByTestId("save-new-reminder"))

  // Now, what should we do after clicking the submit button?

    // If you switch back to development and try to make a Reminder, you'll see that the text box fades and then hides once the Reminder is created.

    // So in our test, we can wait for the input to disappear, and then assert that the new Reminder shows up in the list:

  await waitForElementToBeRemoved(() => screen.getByTestId("new-reminder-text"))

  expect(screen.getByText("Work out")).toBeInTheDocument()

  // if everything worked right, we should have a new Reminder in Mirage's database, and it should be associated to the list we created. We can add these assertions easily enough right alongside our UI assertion:

    // You can think of this as a simple way to verify that your UI is sending the right JSON payload over the wire, without having to drop to the lower level of asserting against HTTP request and response data.

  expect(server.db.reminders.length).toEqual(1)
  expect(server.db.reminders[0].listId).toEqual(list.id)

  // screen.debug()
})