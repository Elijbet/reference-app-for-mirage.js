import "tailwindcss/tailwind.css";
import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import { BrowserRouter, Route } from "react-router-dom";
import { QueryParamProvider } from "use-query-params";
import makeServer from "./server";
import makeFinalServer from "./lib/server-final";

if (
  process.env.NODE_ENV === "development" &&
  typeof makeServer === "function"
) {
  makeServer(); // For people following the tutorial
} else if (
  process.env.NODE_ENV === "production" ||
  process.env.REACT_APP_DEMO
) {
  makeFinalServer(); // For a live demo when deploying to Vercel
  
  // ​Vercel is a cloud platform for static sites and Serverless Functions.

  // Whereas "Heroku is a service for managing stateless web application using the 12 Factor App approach that they pioneered. It has similarities to serverless applications in that much of work of managing and maintaining servers is done for you.

  // However, serverless applications have a number of advantages over Heroku. With Heroku, you need to specify the number of "Dynos" (servers) available to handle your web application. With serverless application, this scaling is handled for you, automatically.

  // Further, you only pay for the resources you consume with Serverless architectures. With Heroku, you pay for your Dynos even when they're sitting idle.

  // Finally, Serverless architectures play more nicely as part of a larger, polyglot architecture. They are deployed directly to your existing cloud provider's account, so you can interact with other parts of your infrastructure."

  //from Serverless.com
}

ReactDOM.render(
  <BrowserRouter>
    <QueryParamProvider ReactRouterRoute={Route}>
      <App />
    </QueryParamProvider>
  </BrowserRouter>,
  document.getElementById("root")
);
