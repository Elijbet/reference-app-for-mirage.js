import { 
  createServer, 
  Model, 
  hasMany, 
  belongsTo,
  RestSerializer,
  Factory,
  trait
} from "miragejs"

// pass in development as environment
export default function (environment = "development") {

  // Open the src/server.js file in your editor, import createServer from miragejs, and export a default function that creates a new server.JSON

    // We import and run this function for you in index.js but in your own apps you can wire this up however you like.

return createServer({

    // using different environments for development and testing

      // we pass in development for rendering the app but for testing purposes we'd like to have a fresh instance of our Mirage server here, one without any data, so we can verify that the empty state shows the "All done!" message. One way we could accomplish this would be to delete all our seeds() data.

      // But there's a better way that doesn't involve changing our dev seeds: we can start our Mirage server in "test" mode using the environment option.

      //see _tests_/app.js for more notes on test environment

    environment,

    // Mirage has a Serializer layer that can help you emulate the kinds of transforms that are commonly applied to APIs, like whether your JSON object keys are camelCased or snake_cased, or how related data gets included in a response.

      // This new serializers key is an option that lets us define serializers on a per-model basis. We could also set an application serializer if we wanted to apply general transformations to all our responses.

      // The RestSerializer class that we're using is one of three named serializers included in Mirage, and is a good starting point for many REST APIs. There's also the ActiveModelSerializer for Rails-like APIs, and the JSONAPISerializer for APIs that follow the JSON:API spec.

    serializers: {

      // 2 customizations to RestSerializer for reminder

      reminder: RestSerializer.extend({

        // First, include: ['list'] tells Mirage that every time it encounters a Reminder model in the response of a route handler, it should include its associated List (if it has one).

        include: ["list"],

        // Second, embed: true tells Mirage how to serialize the included resources. By default they are side-loaded but this API expects them to be embedded.

        embed: true,
      }),
    },

    // model will tell Mirage to create collections for us in its in-memory database

    models: {

      // If you click the button on the left side of the Reminders panel, a side panel will open up that shows the different Lists in this app.

      // The easiest way to mock out endpoints that deal with relational data in Mirage is to use associations. We'll start by using Mirage's association helpers to model this data.

      list: Model.extend({
        reminders: hasMany(),
      }),

      reminder: Model.extend({
        list: belongsTo(),
      }),
    },

    // Mirage includes a Factory layer to help simplify the process of seeding your Mirage server with realistic, relational data.

      // Having to always specify every attribute for every model you create can add a lot of boilerplate to your code, especially during testing. It gets even more complicated when relationships are required for your data to be valid.

      // Factories are the perfect place to encode the constraints of your data, making it easier for you to quickly create valid graphs of data.

    factories: {

      // We can use Factory.extend(config) to pass in a config object, where the keys of the config object correspond to properties on our models.

      list: Factory.extend({
        name(i) {
          return `List ${i}`;
        },

         // Traits let us group attributes and afterCreate logic together under a name, so they're only invoked when we use that name instead of using conditionals.

          // afterCreate(list, server) {
          //   if (!list.reminders.length) {
          //     server.createList('reminder', 5, { list })
          //   }
          // },
          
          // Note that we removed the list.reminders.length check since this logic will now only be invoked if we explicitly call the trait.

        withReminders: trait({
          afterCreate(list, server) {
            server.createList('reminder', 5, { list })
          }
        })
      }),

      // What if we wanted to make it even easier to create a List with many reminders? We can use the afterCreate hook on our List Factory, passing in our newly created list into any new Reminders we create:

      reminder: Factory.extend({
        text(i) {
          return `Reminder ${i}`
        }
      }),
    },

    seeds(server) {

      // The server.create function lets us create new reminders in Mirage's data layer. IDs are automatically assigned, just like when we call reminders.create() in route handlers.

      // server.create("reminder", { text: "Walk the dog" })
      // server.create("reminder", { text: "Take out the trash" })
      // server.create("reminder", { text: "Work out" })

      // Libraries like faker.js also work well in function properties to create even higher fidelity data.

        // The Faker.js library is included with Mirage, and its methods work nicely with factory definitions:

        // Combined with server.createList, we can now easily create many Reminders using our Factory definition:

        // server.createList("reminder", 10);

        // But what if we want to override specific properties that we've defined on our Factory? We can always do that by passing attributes into server.create(). These attributes will override anything defined on our base factories.

        // server.create('reminder', { text: 'Walk the dog' });

        // let homeList = server.create("list", { name: "Home" });
        // server.create("reminder", { list: homeList, text: "Do taxes" });

        // let workList = server.create("list", { name: "Work" });
        // server.create("reminder", { list: workList, text: "Visit bank" });

      // But what if we wanted to bring back some of our more curated, realistic data, either because we're testing something specific or because we want less generic data in development?

      server.create("list", {
        name: "Home",
        reminders: [server.create("reminder", { text: "Do taxes" })],
      });

      // invoke our new trait for that list

      server.create("list", "withReminders")

      // Now if we want to make generic Lists and Reminders, we don't need to specify any attributes:

        // server.create("list", {
        //   reminders: server.createList("reminder", 5),
        // });
    
      server.create("list")

    },

    // The routes() hook is how we define our route handlers, and the this.get() method lets us mock out GET requests. The first argument is the URL we're handling (/api/reminders) and the second argument is a function that responds to our app with some data.

    routes() {

      // Initial static get handler with no defined model.

        // this.get("/api/reminders", () => ({
        //   reminders: [
        //     { id: 1, text: "Walk the dog" },
        //     { id: 2, text: "Take out the trash" },
        //     { id: 3, text: "Work out" },
        //   ],
        // }))

        // If you add new reminders and reload, the new Reminder you created isn't in the list. That's because our GET handler is currently static, and returns the same three hard-coded Reminders every time it runs. We've broken the referential integrity of our application data, and it no longer behaves the same way our production API will.

        // Let's fix this by using Mirage's data layer.
        
        // We'll start by defining a Reminder model.

        // Then we update our GET route handler to return all the Reminders in the database at the time of the request – just like a real server does.
      
      this.get("/api/reminders", (schema) => {

        // The schema argument, which is the first argument passed into all route handlers, is the primary way you interact with Mirage's data layer.

        return schema.reminders.all()
      })
      this.get("/api/lists", (schema, request) => {
        return schema.lists.all()
      })
      this.get("/api/lists/:id/reminders", (schema, request) => {
        let listId = request.params.id
        let list = schema.lists.find(listId)
      
        return list.reminders
      })

      // We're using this.post to handle this request. We're also using the request argument to access the data sent over from our application. The request.requestBody property contains the request body as a JSON string, so after parsing it we log it to the console.

      // When creating an unassociated todo, the attrs are { text: "New todo" }, but an associated todo has attrs like { text: "Respond to Jill", listId: "2" }.

        // When we defined our relationship, Mirage set up special attributes on our models known as foreign keys. These are how it keeps track of which models are associated with each other. Setting a foreign key like listId or reminderIds: [] is enough to update a relationship and have it reflected across all your Mirage route handlers.

        // So, we don't have to update our POST handler to accommodate our UI's List feature, since listId is already being passed into reminders.create().
      
      // let newId = 4

      this.post("/api/reminders", (schema, request) => {
        let attrs = JSON.parse(request.requestBody)
        console.log(attrs)

        // debugger
        // attrs.id = newId++
        // return { reminder: attrs }

        // Here we're getting the attrs from the request just like before, but now we're using the create method from our schema to create new Reminder models.

        return schema.reminders.create(attrs)
      })

      // The colon : in /api/reminders/:id is how we denote a dynamic segment in our URL. We can access the runtime value of the segment via request.params.id. We then use schema to find the corresponding reminder, and then call destroy() on it to remove it from Mirage's database.

      this.delete("/api/reminders/:id", (schema, request) => {
        let id = request.params.id
      
        return schema.reminders.find(id).destroy()
      })
    },
  })
}