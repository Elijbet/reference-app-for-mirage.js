# Mirage Tutorial

This is the Reminders app for the official Mirage tutorial.

Check out [miragejs.com/tutorial](https://miragejs.com/tutorial) to follow along. 

Completed test with commented notes.

Includes notes from the Guides: https://miragejs.com/docs/main-concepts/

Happy mocking!

Part 1 - Static Get Handler 
- Mirage runs in the browser, so there's no new terminal processes to manage
- Returning data from Mirage is as fast and easy as writing frontend code
- You never alter your application code to consume data from Mirage – your app thinks it's talking to a real network

Part 2 – Static POST Handler 
- Mirage can handle POST requests
- Route handlers can use data from the request when constructing the response
- Route handlers should faithfully recreate important details of production server endpoints, like unique IDs

Part 3 – Dynamic Handlers 
- Mirage has a database that acts as a single source of truth for your mock server data
- Route handlers should read from and modify the database via the schema argument in order to preserve the referential integrity of your mock data across route handlers

Part 4 – Seeds 
- Use the seeds hook and server.create to create initial data for your server

Part 5 – Dynamic segments
- Use :segmentName to define a dynamic segment in the URL for a route handler
- Access dynamic segments via request.params.segmentName
- Use schema.* methods like destroy() to maintain data integrity

Part 6 – Relationships
- Mirage lets you define and access relational data
- Use the belongsTo and hasMany helpers to define named relationships on your models
- Use relationships in route handlers to return a model or collection that's associated with other models
- Mirage uses simple foreign keys to keep track of relational data

Part 7 – Serializers
- Mirage's Serializer layer can be used to transform your JSON payloads
- Some common Serializer concerns are changing the shape of your JSON payloads (e.g. whether to include a key in the root), the conventions around formatting for compound words (e.g. camelCase vs. snake_case), and how and when related data should be included

Part 8 – Factories
- Factories are like blueprints that help you easily create graphs of relational data
- You can use static values or functions as attributes
- You can override Factory defaults wherever you call server.create()
- Use the afterCreate hook to perform additional logic, like automatically creating related data for a model
Use Traits to group related attribute and afterCreate logic, and to keep your factories composable

Pat 9 - Testing
- Mirage makes it easy to share your mock server between development and testing
- Use the test environment for your Mirage server when testing so your tests run fast and the database starts out empty
- Take advantage of the fact that your Mirage server is easily accessible within your tests to do things like visit dynamic URLs or assert against changes made to your server's database